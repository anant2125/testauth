package com.lecza.authservice.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class MyController {

	@GetMapping("/public")
	public String greet() {
		return "Welcome to App";
	}
	
	@GetMapping("/private")
	public String greetPrivate() {
		return "Welcome to App with access";
	}
	
	
	@PostMapping("/login")
	public String login(@RequestBody String username, @RequestBody String password) {
		
		
		return "Welcome to App";
	}
	
	
	
	
}
